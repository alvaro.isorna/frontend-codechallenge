import { expect } from 'chai'
// import Vuetify from 'vuetify'
import { mount } from '@vue/test-utils'
import SearchResults from '@/components/SearchResults.vue'

describe('SearchResults.vue', () => {
  it('sets the correct default data', () => {
    expect(typeof SearchResults.data).to.equal('function')
    const defaultData = SearchResults.data()
    expect(defaultData.statusList).to.deep.equal([
      'delivered',
      'error',
      'pending',
      'shipped'
    ])
  })
  it('has no filteredOrders on when created', () => {
    // Proxy for tests interaction with Vuetify, they don't work well together
    const $vuetify = { breakpoint: { mdAndUp: true } }
    const wrapper = mount(SearchResults, {
      propsData: {
        filteredOrders: []
      },
      mocks: {
        $vuetify
      }
    })
    expect(wrapper.props().filteredOrders).to.deep.equal([])
    expect(wrapper.find('td').exists()).to.equal(false)
  })
  it('shows one row on the table when it receives a single result', () => {
    // This test can't be executed due to Vuetify problems
    // const localVue = createLocalVue()
    // localVue.use(Vuetify)
    // const wrapper = mount(SearchResults, {
    //   localVue,
    //   propsData: {
    //     filteredOrders: [{
    //       order_id: 11,
    //       date: '7/24/2018',
    //       last_name: 'Isorna',
    //       first_name: 'Alvaro',
    //       phone: '+34 65432101',
    //       status: 'shipped',
    //       comments: 'Testing comments'
    //     }]
    //   }
    // })
    // expect(wrapper.find('td').exists()).to.equal(true)
  })
})
