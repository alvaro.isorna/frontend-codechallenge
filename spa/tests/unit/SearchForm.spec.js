import { expect } from 'chai'
import SearchForm from '@/components/SearchForm.vue'

describe('SearchForm.vue', () => {
  it('sets the correct default data', () => {
    expect(typeof SearchForm.data).to.equal('function')
    const defaultData = SearchForm.data()
    expect(defaultData.locale).to.equal('en-US')
    expect(defaultData.statusList).to.deep.equal([
      'delivered',
      'error',
      'pending',
      'shipped'
    ])
  })
  it('formats date from YYYY-MM-DD to MM/DD/YYYY', () => {
    expect(typeof SearchForm.methods.formatDate).to.equal('function')
    expect(SearchForm.methods.formatDate('2018-03-20')).to.equal('03/20/2018')
  })
  it('parses date from MM/DD/YYYY to YYYY-MM-DD', () => {
    expect(typeof SearchForm.methods.parseDate).to.equal('function')
    expect(SearchForm.methods.parseDate('03/20/2018')).to.equal('2018-03-20')
  })
})
