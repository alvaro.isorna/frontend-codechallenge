import Vue from 'vue'

const PUSH_ORDER = 'PUSH_ORDER'
const PUSH_API_MESSAGE = 'PUSH_API_MESSAGE'
const RESET_RESULTS = 'RESET_RESULTS'

const state = {
  filteredOrders: [],
  apiMessage: null
}

const getters = {
  filteredOrders: ({ filteredOrders }) => filteredOrders,
  apiMessage: ({ apiMessage }) => apiMessage
}

const mutations = {
  [PUSH_ORDER] (orderState, order) {
    state.filteredOrders.push(order)
  },
  [PUSH_API_MESSAGE] (orderState, error) {
    state.apiMessage = error
  },
  [RESET_RESULTS] () {
    state.filteredOrders = []
    state.apiMessage = null
  }
}

const actions = {
  resetResults ({ commit }) {
    commit(RESET_RESULTS)
  },
  searchOrders ({ commit }, queryParams) {
    // Reset filteredOrders so that previous search is not mixed with current one
    commit(RESET_RESULTS)
    Vue.http.get('http://goi.front-challenge.s3-website-eu-west-1.amazonaws.com/').then((response) => {
      let orders = response.body

      // Apply query params before populating filteredResults
      if (queryParams.dateForAPI) {
        orders = orders.filter((order) => {
          return queryParams.dateForAPI === order.date
        })
      }

      if (queryParams.status) {
        orders = orders.filter((order) => {
          return queryParams.status === order.status
        })
      }

      if (queryParams.searchText) {
        orders = orders.filter((order) => {
          return (order.comments.indexOf(queryParams.searchText) >= 0 ||
            order.first_name.indexOf(queryParams.searchText) >= 0 ||
            order.last_name.indexOf(queryParams.searchText) >= 0 ||
            order.phone.indexOf(queryParams.searchText) >= 0)
        })
      }

      // Commit filteredResults after query params are sorted out
      orders.forEach((order) => commit(PUSH_ORDER, order))
    }, (response) => {
      // Show notification to user:
      commit(PUSH_API_MESSAGE, {type: 'error', message: `API ERROR: ${response.status} ${response.statusText}`})
    })
  },
  editOrder ({ commit }, editedItem) {
    Vue.http.get(`http://goi.front-challenge.s3-website-eu-west-1.amazonaws.com/${editedItem.order_id}`).then((response) => {
      commit(PUSH_API_MESSAGE, {type: 'info', message: `API SUCCESS: ${response.status} ${response.statusText}`})
    }, (response) => {
      // Show notification to user:
      commit(PUSH_API_MESSAGE, {type: 'error', message: `API ERROR: ${response.status} ${response.statusText}`})
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
