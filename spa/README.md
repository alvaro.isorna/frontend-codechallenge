# GOI - Gestión de pedidos

VueJS project created with [vue-cli](https://cli.vuejs.org/), using [Vuetify](https://vuetifyjs.com/) as UI framework.

## Project setup
```
npm install
```

## Recommended project init for development
```
vue ui
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
